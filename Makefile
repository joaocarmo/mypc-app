DB_DATA=db_data

.PHONY: build clean down install prod_build prod_up up

SHELL := /bin/bash
PATH := ./node_modules/.bin:$(PATH)

all: node_modules

build:
	docker compose build

clean:
	rm -rf $(DB_DATA)

down:
	docker compose down -v

install:
	yarn install --frozen-lockfile

node_modules: package.json
	yarn install --frozen-lockfile
	touch $@

prod_build:
	docker compose -f docker-compose.yml -f docker-compose.prod.yml build

prod_up:
	mkdir -p $(DB_DATA)
	docker compose -f docker-compose.yml -f docker-compose.prod.yml up -d

up:
	mkdir -p $(DB_DATA)
	docker compose up -d
