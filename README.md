# mypc-app

MyPC app POC.

## Preview

This POC app will show up something like this.

![APP Preview][preview-img]

## Development

```sh
# Build the images
make build

# Start the development container (in the background)
make up

# Stop the development container and remove its volumes
make down
```

The frontend app will be available at <http://localhost:3000> and is ready for
development.

The backend app will be available at <http://localhost:3001> and is ready for
development.

The backend app will also be available at <http://localhost:3000/api>.

### Database

An instance of [MongoDB](https://www.mongodb.com/) will be used to persist app
data. A web-based MongoDB admin interface
([mongo-express](https://github.com/mongo-express/mongo-express)) will be
available at <http://localhost:8081>.

```txt
# DB
USERNAME: mongo_user
PASSWORD: mongo_pass

# mongo-express
USERNAME: admin
PASSWORD: pass
```

## Production

There is no need to build the images for production, because they've been built
using GitLab's CI/CD and are already ready to execute.

```sh
# Start the production container (in the background)
make prod_up

# Stop the production container and remove its volumes
make down
```

The app will be available at <http://localhost> (default port `80`).

<!-- References -->

[preview-img]: ./images/preview.png
